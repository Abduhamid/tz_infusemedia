<?php

require_once('config.php');

class Database
{
    private static $connection = null;

    private function __construct()
    {

    }

    private function __clone()
    {

    }

    protected static function getConnection()
    {
        if (self::$connection == null)
        {
            try{
                $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
                self::$connection = new PDO($dsn, DB_USER, DB_PASS);
                self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                self::$connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            }catch(Exception $e){
                throw new Exception($e->getMessage());
            }
        }

        return self::$connection;
    }
    
    public static function Insert($statement = '', $parameters = [])
    {
        try{
            self::executeStatement($statement, $parameters);

            return self::getConnection()->lastInsertId();
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }

    public static function Select($statement = '', $parameters = [])
    {
        try{
            $stmt = self::executeStatement($statement, $parameters);

            return $stmt->fetchAll();
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }

    public static function Update($statement = '', $parameters = [])
    {
        try{

            return self::executeStatement($statement, $parameters);
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }

    public static function Remove($statement = '', $parameters = [])
    {
        try{

            return self::executeStatement($statement, $parameters);
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }

    private static function executeStatement($statement = '', $parameters = [])
    {
        try{
            $stmt = self::getConnection()->prepare($statement);
            $stmt->execute($parameters);

            return $stmt;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
}