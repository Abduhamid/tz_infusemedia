<?php

require_once('Database.php');

class View
{
    private $table = 'views';

    public function insert($ipAddress, $userAgent, $pageUrl, $viewDate)
    {
        $parameters = [
            'ip_address' => $ipAddress,
            'user_agent' => $userAgent,
            'view_date' => $viewDate,
            'page_url' => $pageUrl,
            'views_count' => 1,
        ];
        $statement = 'INSERT INTO ' . $this->table . ' (ip_address, user_agent, view_date, page_url, views_count) 
            VALUES (:ip_address, :user_agent, :view_date, :page_url, :views_count)';

        return Database::Insert($statement, $parameters);
    }

    public function update($id, $ipAddress, $userAgent, $pageUrl, $viewDate, $viewsCount)
    {
        $parameters = [
            'id' => $id,
            'ip_address' => $ipAddress,
            'user_agent' => $userAgent,
            'view_date' => $viewDate,
            'page_url' => $pageUrl,
            'views_count' => $viewsCount,
        ];
        $statement = 'UPDATE ' . $this->table . ' SET ip_address = :ip_address, user_agent = :user_agent, 
            view_date = :view_date, page_url = :page_url, views_count = :views_count WHERE id = :id';

        return Database::Update($statement, $parameters);
    }

    public function createOrUpdate($ipAddress, $userAgent, $pageUrl, $viewDate)
    {
        $result = $this->findOne($ipAddress, $userAgent, $pageUrl);
        if (isset($result[0]['id'])){

            return $this->update($result[0]['id'], $ipAddress, $userAgent, $pageUrl, $viewDate,
                $result[0]['views_count'] + 1);
        } else{

            return $this->insert($ipAddress, $userAgent, $pageUrl, $viewDate);
        }
    }

    private function findOne($ipAddress, $userAgent, $pageUrl)
    {
        $parameters = [
            'ip_address' => $ipAddress,
            'user_agent' => $userAgent,
            'page_url' => $pageUrl,
        ];
        $statement = 'SELECT * FROM ' . $this->table . ' WHERE ip_address = :ip_address AND user_agent = :user_agent AND 
            page_url = :page_url LIMIT 1';

        return Database::select($statement, $parameters);
    }
}