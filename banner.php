<?php

require_once('View.php');

$view = new View();
$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
$url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$url = $_SERVER['HTTP_REFERER'] ?? $url;
$view->createOrUpdate($_SERVER['REMOTE_ADDR'], $_SERVER['HTTP_USER_AGENT'], $url, date('Y-m-d H:i:s'));
